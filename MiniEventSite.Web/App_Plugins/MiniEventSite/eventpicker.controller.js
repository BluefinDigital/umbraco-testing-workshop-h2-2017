﻿(function () {

    function EventPickerController(scope, http) {
        http.get("https://eventsite.com/events/json")
            .then(function (response) {
                scope.events = response.data;
            });

        scope.events = [];
    }

    angular.module("mini.event.site").controller("eventpicker.controller", [
        "$scope",
        "$http",
        EventPickerController
    ]);

}());