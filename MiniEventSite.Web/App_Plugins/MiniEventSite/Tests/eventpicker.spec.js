﻿/* globals: describe, beforeEach, afterEach, it, expect, module, inject */
describe("Eventpicker", function () {
    var rootScope,
        element;

    beforeEach(module("mini.event.site"));
    beforeEach(setupUiPreview);

    beforeEach(inject(function ($compile, $rootScope, $httpBackend) {
        var link = $compile('<events-picker model="model"></events-picker>'),
            scope;
        rootScope = $rootScope;
        rootScope.model = {
            value: ""
        }
        scope = $rootScope.$new();
        element = link(scope);
        $("#sample-ui").append(element);

        $httpBackend
            .whenGET("https://eventsite.com/events/json")
            .respond(createSampleData());

        scope.$digest();
        $httpBackend.flush();
    }));

    it("Shows events from EventSite in a dropdown", function () {
        expect($("option", element).length).toBe(3);
        expect($("option:eq(1)", element).text()).toBe("Bigger concert than ever");
    });

    it("Sets the value of scope's model", function () {
        $("option:eq(1)", element).prop("selected", true);
        $("select", element)[0].dispatchEvent(new Event("change"));
        expect(rootScope.model.value).toBe("124");
    });

    function setupUiPreview() {
        $("#sample-ui").remove();
        $("<div id=\"sample-ui\"></div>").appendTo("body");
    }

    function createSampleData() {
        return [
            {
                id: "124",
                name: "Bigger concert than ever",
                time: 1577836799
            },
            {
                id: "123",
                name: "Concert of your life",
                time: 2524607999
            }
        ];
    }
});