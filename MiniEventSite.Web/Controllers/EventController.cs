﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using MiniEventSite.Web.Models;
using MiniEventSite.Web.Models.DocumentTypes;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace MiniEventSite.Web.Controllers
{
    public class EventController : SurfaceController
    {
        private readonly HttpClient httpClient;

        public EventController()
        {
            httpClient = new HttpClient();
        }

        public EventController(UmbracoContext umbracoContext, HttpClient httpClient)
            : base(umbracoContext)
        {
            this.httpClient = httpClient;
        }

        public async Task<PartialViewResult> DisplayEvent()
        {
            var contentPage = (ContentPage)CurrentPage;
            var eventId = contentPage.Event;
            var response = await httpClient.GetAsync("http://eventsite/event/" + eventId);
            var data = await response.Content.ReadAsAsync<EventDto>();

            var model = new DisplayEventModel
            {
                Id = data.Id,
                Name = data.Name,
                Time = DateTimeOffset.FromUnixTimeSeconds(data.Time).DateTime
            };

            return PartialView("DisplayEvent", model);
        }
    }
}