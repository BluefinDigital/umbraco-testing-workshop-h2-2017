﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using Examine;
    using MiniEventSite.Web.Models;
    using MiniEventSite.Web.Models.DocumentTypes;
    using umbraco;
    using Umbraco.Core;
    using Umbraco.Core.Models;
    using Umbraco.Web;
    using Umbraco.Web.Mvc;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Partials/DisplayEvent.cshtml")]
    public partial class _Views_Partials_DisplayEvent_cshtml : UmbracoViewPage<DisplayEventModel>
    {
        public _Views_Partials_DisplayEvent_cshtml()
        {
        }
        public override void Execute()
        {
WriteLiteral("<div");

WriteLiteral(" class=\"event\"");

WriteLiteral(">\r\n    <h3><a");

WriteAttribute("href", Tuple.Create(" href=\"", 83), Tuple.Create("\"", 122)
, Tuple.Create(Tuple.Create("", 90), Tuple.Create("http://eventsite/event/", 90), true)
            
            #line 4 "..\..\Views\Partials\DisplayEvent.cshtml"
, Tuple.Create(Tuple.Create("", 113), Tuple.Create<System.Object, System.Int32>(Model.Id
            
            #line default
            #line hidden
, 113), false)
);

WriteLiteral(">");

            
            #line 4 "..\..\Views\Partials\DisplayEvent.cshtml"
                                              Write(Model.Name);

            
            #line default
            #line hidden
WriteLiteral("</a></h3>\r\n    <p>");

            
            #line 5 "..\..\Views\Partials\DisplayEvent.cshtml"
  Write(Model.Time.ToString("f"));

            
            #line default
            #line hidden
WriteLiteral("</p>\r\n</div>\r\n");

        }
    }
}
#pragma warning restore 1591
